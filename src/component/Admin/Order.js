import { Button, Container, Box, Typography, Modal, TablePagination, ButtonGroup, TextField, MenuItem, Alert, Snackbar, AlertTitle, Grid, CircularProgress} from "@mui/material";
import { styled } from '@mui/material/styles';
import {Table, TableHead, TableRow, TableBody, TableContainer, Paper, TableCell, tableCellClasses } from '@mui/material/';
import { blue, green, grey, red } from "@mui/material/colors";
import { useState, useEffect } from "react";
import HeaderComponent from "../Header/HeaderComponent";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";

//====================QUẢN LÝ ARRAY===============

const statusArr = [
    {
        label: 'Open',
        value: 'open'
    },
    {
        label: 'Đã hủy',
        value: 'cancel'
    },
    {
        label: 'Đã xác nhận',
        value: 'confirmed'
    },
]

//=================END-QUẢN LÝ ARRAY==============

//==========QUẢN LÝ CÁC COMPONENT-STYLED============
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // Kẻ viền cho bảng
    '& td, & th': {
        border: `1px solid ${grey[300]}`
    }
  }));

  const StyledTableCell = styled(TableCell)(() => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: grey[0],
        color: grey[900],
        fontWeight:'bold',
        border: `1px solid ${grey[300]}`,
        textAlign: 'center'
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        textAlign: 'center'      
    },
    }));

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };
//==========END-QUẢN LÝ CÁC COMPONENT-STYLED============


function Order () {
    //=============QUẢN LÝ STATE=================//
    //Thông tin xử lý arr R-Read
    const [rows, setRows] = useState([]);
    const [selectRow, setSelectRow] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    //Thông tin xử lý button sửa U-Update
    const [openEdit, setOpenEdit] = useState(false);
    const handleCloseEdit = () => setOpenEdit(false);
    //Thông tin xử lý button thêm C-Create
    const [openCreate, setOpenCreate] = useState(false);
    const handleCloseCreate = () => setOpenCreate(false);
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [address, setAddress] = useState('');
    const [note, setNote] = useState('');
    //Thông tin xử lý button xóa D-Delete
    const [openDelete, setOpenDelete] = useState(false);
    const handleCloseDelete = () => setOpenDelete(false);
    //Thông tin xử lý các trường trong thêm order
    const [voucher, setVoucher] = useState(null)
    const [open, setOpen] = useState(false);
    const [message, setMsessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const {SizeArray} = useSelector((reduxData) => reduxData.pizzaSizeReducer)
    const {TypeArray} = useSelector((reduxData) => reduxData.pizzaTypeReducer)
    const {drinksArray} = useSelector((reduxData) => reduxData.drinkListReducer)
    const [selectPizza, setSelectPizza] = useState([]);
    const [selectType, setSelectType] = useState([]);
    const [selectDrink, setSelectDrink] = useState([]);
    const [status, setStatus] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    
    //=============END-QUẢN LÝ STATE=================//

    //=============QUẢN LÝ API=================//  
    //async await API
    const fetchAPI = async(url, requestOptions) => {
        const response = await fetch(url, requestOptions);
        return response;
    }
    //========== READ - R================
    //Get API danh sách đơn hàng
    useEffect(()=>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/", requestOptions)
        .then(response => response.json())
        .then(result => {
            setRows(result);
        })
        .catch(error => console.log('error', error));        
    },[])

    //Get API Drinks
    const dispatch = useDispatch();
    useEffect(() => {
        let axiosAPI = async (config) => {
          let response = await axios.request(config);
          return response;        
        }
        let config = {
          method: 'get',
          maxBodyLength: Infinity,
          url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
          headers: {},
        };
      
        axiosAPI(config)
          .then((response) => {
            //console.log(response.data)
            dispatch({
              type: 'SET_ARRAY_DRINKS',
              payload: {
                drinksArray: response.data
              }
            });
          })
          .catch((error) => {
            //console.log(error);
          });
      }, [dispatch]);
    //=============END-QUẢN LÝ API=================//


    //=============QUẢN LÝ CÁC EVENT===============//
    //Xử lý button tại index trong row console log information từng row
    const handleButtonEdit = (row) => {
        console.log(row)
        setOpenEdit(true)   
        setSelectRow(row)
    };

    const handleButtonDelete = (row) => {
        console.log(row)
        setOpenDelete(true)   
        setSelectRow(row)
    };

    //Xử lý handle change Page
    const handleChangePage = (event, value) => {
        setPage(value)
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const handleAddUser = () => {
        setOpenCreate(true)
    };

    const onChangeFullName = (event) => {
        console.log(event.target.value);
        setFullName(event.target.value);                        
    };

    const onChangeEmaill = (event) => {
        console.log(event.target.value);
        setEmail(event.target.value);      
    };
    
    const onChangePhone = (event) => {
        console.log(event.target.value);
        setPhone(event.target.value);       
    };

    const onChangeNote = (event) => {
        console.log(event.target.value);
        setNote(event.target.value);
    }

    const onChangeAddress = (event) => {
        console.log(event.target.value);
        setAddress(event.target.value);
    }

    const handleChangeCombo = (index) => {
        const newSelectPizza = SizeArray[index]
        setSelectPizza(SizeArray[index])
        console.log(newSelectPizza)
    }

    const handleChangeType = (index) => {
        const newSelectType = TypeArray[index]
        setSelectType(TypeArray[index])
        console.log(newSelectType)
    }

    const handleSelectDrink = (index) => {
        const newSelectDrinks = drinksArray[index]
        setSelectDrink(drinksArray[index]);
        console.log(newSelectDrinks)
    }

    const handleChangeVoucher = (event) => {
        setVoucher(event.target.value);
    }

    const handleSelectStatus = (index) => {
        const newSelectStatus = statusArr[index]
        setStatus(statusArr[index]);
        console.log(newSelectStatus)
    }

    const handleConfirmCreate = () => {
        const newOrder = JSON.stringify ({
            kichCo: selectPizza.pizzaSize,
            duongKinh: selectPizza.duongKinh,
            suon: selectPizza.suongNuong,
            salad: selectPizza.salad,
            loaiPizza: selectType.tenTypePizza,
            idVourcher: voucher,
            idLoaiNuocUong: selectDrink.maNuocUong,
            soLuongNuoc: selectPizza.nuocNgot,
            hoTen: fullName,
            thanhTien: selectPizza.price + (selectDrink.donGia * selectPizza.nuocNgot),
            email: email,
            soDienThoai: phone,
            diaChi: address,
            loiNhan: note           
        })
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'POST',
            body:newOrder,
            headers: myHeaders,
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/", requestOptions)
        .then(result => {
            if(result.status === 201){
                setMsessage('Tạo mới thành công đơn hàng');
                setOpen(true);
                setSeverity('success');
                setTimeout(() => {
                    window.location.reload();
                }, 2000); // Đợi 2 giây trước khi tải lại trang
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Tạo đơn hàng mới không thành công, bạn vui lòng check lại')
            setSeverity('error')
        });
        handleCloseCreate();
        window.location.reload();
    };

    const handleUpdateUser = (paramSelectRow) => {
        const updateUser = JSON.stringify ({
            kichCo: selectPizza.pizzaSize,
            duongKinh: selectPizza.duongKinh,
            suon: selectPizza.suongNuong,
            salad: selectPizza.salad,
            loaiPizza: selectType.tenTypePizza,
            idVourcher: voucher,
            idLoaiNuocUong: selectDrink.tenNuocUong,
            soLuongNuoc: selectPizza.nuocNgot,
            hoTen: fullName,
            thanhTien: selectPizza.price + (selectDrink.donGia * selectPizza.nuocNgot),
            email: email,
            soDienThoai: phone,
            diaChi: address,
            loiNhan: note,
            trangThai: status.value
        })
        
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'PUT',
            body:updateUser,
            headers: myHeaders,
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramSelectRow.id, requestOptions)
        .then(response => {
            console.log(response);
            if (response.status === 200) {
                setMsessage('Cập nhât thông tin đơn hàng thành công');
                setOpen(true);
                setSeverity('success')
                setTimeout(() => {
                    window.location.reload();
                }, 2000); // Đợi 2 giây trước khi tải lại trang
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Cập nhật đơn hàng không thành công, bạn vui lòng check lại')
            setSeverity('error')
        });
        handleCloseEdit();
    };

    const handleDeleteUser = (paramSelectRow) => {
        //console.log(paramSelectRow.id)
        setIsLoading(true);// Bắt đầu hiển thị Loading
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramSelectRow.id, requestOptions)
        .then(response => {
            console.log(response);
            if (response.status === 204) {
                setMsessage('Xóa thông tin thành công user');
                setOpen(true);
                setSeverity('success')
                setTimeout(() => {
                    setIsLoading(false);//Kết thúc hiện thị Loading
                    window.location.reload();
                }, 2000); // Đợi 2 giây trước khi tải lại trang
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Xóa không thành công, bạn vui lòng check lại')
            setSeverity('error')
            setIsLoading(false)//Kết thúc hiện thị Loading
        });
        handleCloseDelete();
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    };
    
//=============END-QUẢN LÝ CÁC EVENT===============//
    return(
        <> 
        <HeaderComponent/>
        <Typography align="center" sx={{mt: 15, fontFamily: 'REM', fontWeight: 'bold'}} variant="h4">QUẢN LÝ ĐƠN HÀNG</Typography>
        <Container maxWidth={'xl'} sx={{mt: 3}}>
            <Button sx={{
                mb :3
            }} 
            color="success" 
            variant="contained"
            onClick={handleAddUser}
            >Thêm đơn hàng</Button>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 1000 }} aria-label="customized table">
                <TableHead>
                <TableRow>
                    <StyledTableCell>Mã đơn hàng</StyledTableCell>
                    <StyledTableCell>Họ và tên</StyledTableCell>
                    <StyledTableCell>Email</StyledTableCell>
                    <StyledTableCell>Phone</StyledTableCell>
                    <StyledTableCell>Địa chỉ</StyledTableCell>
                    <StyledTableCell>Combo Pizza</StyledTableCell>
                    <StyledTableCell>Loại Pizza</StyledTableCell>
                    <StyledTableCell>Mã giảm giá</StyledTableCell>
                    <StyledTableCell>Thành tiền</StyledTableCell>
                    <StyledTableCell>Ghi chú</StyledTableCell>
                    <StyledTableCell>Action</StyledTableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => (
                        <StyledTableRow key={row.orderCode}>
                            <StyledTableCell>{row.orderCode}</StyledTableCell>
                            <StyledTableCell>{row.hoTen}</StyledTableCell>
                            <StyledTableCell>{row.email}</StyledTableCell>
                            <StyledTableCell>{row.soDienThoai}</StyledTableCell>
                            <StyledTableCell>{row.diaChi}</StyledTableCell>
                            <StyledTableCell>{row.kichCo}</StyledTableCell>
                            <StyledTableCell>{row.loaiPizza}</StyledTableCell>
                            <StyledTableCell>{row.idVourcher}</StyledTableCell>
                            <StyledTableCell>{row.thanhTien}</StyledTableCell>
                            <StyledTableCell>{row.loiNhan}</StyledTableCell>
                            <StyledTableCell>
                                <Button variant="contained" onClick={() => handleButtonEdit(row)}>Sửa</Button>
                                <Button sx={{
                                    ml: 1,
                                    backgroundColor: red[700],
                                    '&:hover': {
                                    bgcolor: red[900]
                                    }
                                }}
                                variant="contained" 
                                onClick={()=>handleButtonDelete(row)}
                                >Xóa
                                </Button>
                                </StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />

            {/*========QUẢN LÝ MODAL CREATE====== */}
            <Modal
                open={openCreate}
                onClose={handleCloseCreate}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Đơn hàng mới
                </Typography>
                <TextField onChange={onChangeFullName} fullWidth sx={{mt: 2}} label={'Họ và tên'}/>
                <TextField type="email" onChange={onChangeEmaill} fullWidth sx={{mt: 2}} label={'Email'}/>
                <TextField onChange={onChangePhone} fullWidth sx={{mt: 2}} label={'Số điện thoại'}/>
                <TextField onChange={onChangeAddress} fullWidth sx={{mt: 2}} label={'Địa chỉ'}/>
                <Grid container>
                    <Grid sx={{pr: 1}} item xs={6}>
                        <TextField 
                        defaultValue={'M (Medium)'}
                        select 
                        helperText='Chọn combo Pizza' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Combo'}
                        >
                            {SizeArray.map((combo, index) => (
                                <MenuItem onClick={() => handleChangeCombo(index)} key={index} value={combo.pizzaSize}>
                                    {combo.pizzaSize}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                    <Grid sx={{pl: 1}} item xs={6}>
                        <TextField 
                        defaultValue={'Hải sản'}
                        select 
                        helperText='Chọn loại Pizza' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Loại Pizza'}
                        >
                            {TypeArray.map((type, index) => (
                                <MenuItem onClick={() => handleChangeType(index)} key={index} value={type.tenTypePizza}>
                                    {type.tenTypePizza}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid sx={{pr: 1}} item xs={6}>
                        <TextField  
                        helperText='Nhập mã giảm giá của bạn' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Mã giảm giá'}
                        onChange={handleChangeVoucher}
                        >

                        </TextField>
                    </Grid>
                    <Grid sx={{pl: 1}} item xs={6}>
                        <TextField 
                        defaultValue={'TRASUA'}
                        select 
                        helperText='Chọn loại Nước' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Loại Nước'}
                        >
                            {drinksArray.map((type, index) => (
                                <MenuItem onClick={() => handleSelectDrink(index)} key={index} value={type.maNuocUong}>
                                    {type.tenNuocUong}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                </Grid>                
                <TextField multiline rows={3} onChange={onChangeNote} fullWidth sx={{mt: 2}} label={'Ghi chú'}/>
                <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: green[900],
                            '&:hover': {
                                bgcolor: green[600]
                            },
                            mr: 8
                        }}
                        onClick={handleConfirmCreate}
                        >
                            Xác nhận
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseCreate}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {/*========END-QUẢN LÝ MODAL CREATE====== */}

            {/*========QUẢN LÝ MODAL EDIT====== */}
            <Modal
                open={openEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                id="U - Update"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Thông tin chi tiết đơn hàng
                </Typography>
                <TextField onChange={onChangeFullName} fullWidth sx={{mt: 2}} label={'Mã đơn hàng'} defaultValue={selectRow.orderCode}/>
                <TextField onChange={onChangeFullName} fullWidth sx={{mt: 2}} label={'Họ và tên'} defaultValue={selectRow.hoTen}/>
                <TextField type="email" onChange={onChangeEmaill} fullWidth sx={{mt: 2}} label={'Email'} defaultValue={selectRow.email}/>
                <TextField onChange={onChangePhone} fullWidth sx={{mt: 2}} label={'Số điện thoại'} defaultValue={selectRow.soDienThoai}/>
                <TextField onChange={onChangeAddress} fullWidth sx={{mt: 2}} label={'Địa chỉ'} defaultValue={selectRow.diaChi}/>
                <Grid container>
                    <Grid sx={{pr: 1}} item xs={6}>
                        <TextField 
                        defaultValue={selectRow.kichCo}
                        select 
                        helperText='Chọn combo Pizza' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Combo'}
                        >
                            {SizeArray.map((combo, index) => (
                                <MenuItem onClick={() => handleChangeCombo(index)} key={index} value={combo.pizzaSize}>
                                    {combo.pizzaSize}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                    <Grid sx={{pl: 1}} item xs={6}>
                        <TextField 
                        defaultValue={selectRow.loaiPizza}
                        select 
                        helperText='Chọn loại Pizza' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Loại Pizza'}
                        >
                            {TypeArray.map((type, index) => (
                                <MenuItem onClick={() => handleChangeType(index)} key={index} value={type.tenTypePizza}>
                                    {type.tenTypePizza}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid sx={{pr: 1}} item xs={6}>
                        <TextField  
                        helperText='Nhập mã giảm giá của bạn' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Mã giảm giá'}
                        onChange={handleChangeVoucher}
                        >

                        </TextField>
                    </Grid>
                    <Grid sx={{pl: 1}} item xs={6}>
                        <TextField 
                        defaultValue={selectRow.idLoaiNuocUong}
                        select 
                        helperText='Chọn loại Nước' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Loại Nước'}
                        >
                            {drinksArray.map((drink, index) => (
                                <MenuItem onClick={() => handleSelectDrink(index)} key={index} value={drink.maNuocUong}>
                                    {drink.tenNuocUong}
                                </MenuItem>
                            ))}
                            </TextField>
                    </Grid>
                </Grid>
                <TextField 
                        select 
                        defaultValue={selectRow.trangThai}
                        helperText='Trạng thái đơn hàng' 
                        fullWidth 
                        sx={{mt: 2}} 
                        label={'Trạng thái đơn hàng'}
                        >
                            {statusArr.map((status, index) => (
                                <MenuItem onClick={() => handleSelectStatus(index)} key={index} value={status.value}>
                                    {status.label}
                                </MenuItem>
                            ))}
                            </TextField>                
                <TextField multiline value={selectRow.loiNhan} rows={3} onChange={onChangeNote} fullWidth sx={{mt: 2}} label={'Ghi chú'}/>
                <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: blue[900],
                            '&:hover': {
                                bgcolor: blue[600]
                            },
                            mr: 8
                        }}
                            onClick={() => handleUpdateUser(selectRow)}
                        >
                            Update User
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseEdit}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {/*========END-QUẢN LÝ MODAL EDIT====== */}


            {/*========QUẢN LÝ MODAL DELETE====== */}
            <Modal
            open={openDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography variant="h6" align="center">
                        {'Xác nhận muốn xóa thông tin này'}
                    </Typography>
                    <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: red[600],
                            '&:hover': {
                                bgcolor: red[900]
                            },
                            mr: 8
                        }}
                            onClick={() => handleDeleteUser(selectRow)}
                            >
                            Xác nhận
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseDelete}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {isLoading && <Modal
             open={true}
             aria-labelledby="modal-modal-title"
             aria-describedby="modal-modal-description"
            >
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                    <CircularProgress />
                </Box>
            </Modal>}
            {/*========END-QUẢN LÝ MODAL DELETE====== */}
            <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
                    {severity === 'success' ? <AlertTitle>Success</AlertTitle>: <AlertTitle>Error</AlertTitle>}
                    {message}
                </Alert>
            </Snackbar>
        </Container>
        </>
    )
}
export default Order;