import Info from "./Info";
import Lading from "./Lading";
import { Container, Typography } from "@mui/material";
import PizzaSize from "./PizzaSize";
import PizzaType from "./PizzaType";
import DrinkSelect from './DrinkSelect';
import FormSubmit from "./FormSubmit";

function ContentComponent () {
    return(
        <>
        <Container style={{paddingTop: '6rem'}}>
            <Typography variant="h4"
                sx={{
                    fontFamily: 'REM',
                    fontWeight:'700',
                    color: '#ff784e'
                }}
                >
                PIZZA 365
            </Typography>
            <Typography variant="body3"
                sx={{
                    fontFamily: 'REM',
                    fontWeight:'700',
                    color: '#ff784e',
                    fontStyle: 'italic'
                }}
                >
                Trully Italian!
            </Typography>
            <Lading/>
            <Info/>
            <PizzaSize/>
            <PizzaType/>
            <DrinkSelect/>
            <FormSubmit/>
        </Container>
        
        </>
    )
}
export default ContentComponent;