import { Button, Link, Typography} from "@mui/material";
import { deepOrange, grey } from "@mui/material/colors";
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';

function Copyright(props) { 
    return (
      <Typography variant="body2" color={grey[50]} {...props}>
        {'Copyright © '}
        <Link color="inherit" href="https://localhost:3000/">
          Pizza365
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }
function Footer () {
    const handleClickScrollToTop = () => {
        window.scrollTo({ 
            top: '15%', 
            behavior:'smooth' 
        });
    };

    return (
        <div
        style={{
            backgroundColor: deepOrange[400],
            color: grey[50],
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}
      >
        <h3>Footer</h3>
        <Button sx={{
            color: grey[50],
            backgroundColor: grey[600],
            '&:hover': {
                bgcolor: grey[800],
            },
            fontWeight: 'bolder'
        }}
            onClick={handleClickScrollToTop}
        >
            <ArrowCircleUpIcon/>{' To the Top'}</Button>
        <Copyright sx={{ mt: 2}} />
      </div>
    )
}

export default Footer;