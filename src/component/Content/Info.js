import { Grid, Typography } from "@mui/material";
import { yellow , deepOrange} from "@mui/material/colors";

const InfoWhy = [
    {
        title: 'Đa dạng',
        content: 'Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay',
        color: `${yellow[200]}`,
    },
    {
        title: 'Chất lượng',
        content: 'Nguyện liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.',
        color: `${yellow[500]}`
    },
    {
        title: 'Hương vị',
        content: 'Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.',
        color: `${deepOrange[300]}`
    },
    {
        title: 'Dịch vụ',
        content: 'Nhân viên thân thiện nhà hàng hiện đại.Dịch vụ giao hàng nhanh chất lượng, tân tiến',
        color: `${yellow[900]}`,
    },
]

function Info () {
    return(
        <div>
            <div style={{
                textAlign: 'center',
                color: '#ff784e',
            }}>
                <h1>Tại sao lại Pizza 365</h1>
                <hr style={{
                    width:'20%',
                    border: '1px solid'
                }}></hr>
            </div>
            <div style={{
                paddingTop : "4%",
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}>
                <Grid sx={{
                    border: '0.1em solid #ff784e', 
                    }}
                container spacing={{xs:1}} columns={{xs: 3, sm: 6, md: 12}}>
                    {InfoWhy.map((item, index) => {
                        return(
                            <Grid sx={{
                                backgroundColor: `${item.color}`,
                                padding: '2em 2em',                      
                                }}
                                item xs={3}
                                key={index}>
                                <Typography variant="h5"
                                    sx={{
                                        fontWeight: '700',
                                        fontFamily: 'REM',
                                    }}
                                >
                                    {item.title}
                                </Typography>
                                <Typography variant="body1"
                                    sx={{
                                        fontSize:"1rem",
                                        lineHeight :"normal!important",
                                        fontFamily: 'REM',
                                        marginTop: '1em'
                                    }}
                                >
                                    {item.content}
                                </Typography>
                            </Grid>
                        )
                    })}
                </Grid>
            </div>        
        </div>
    )
}

export default Info;