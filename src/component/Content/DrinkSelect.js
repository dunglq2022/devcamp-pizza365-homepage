import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Snackbar, Alert } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

const ITEM_HEIGHT = 30;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function DrinkSelect () {
    const dispatch = useDispatch();
    const {selectDrink, drinksArray} = useSelector((reduxData) => reduxData.drinkListReducer); 
    const [open, setOpen] = useState(false)
    
    const handleSelcectDrinks = (index) => {
      dispatch({
        type:'SELECT_DRINK',
        payload:{
          selectDrink: drinksArray[index]
        }
      })
      handleClick();
    }

    useEffect(() => {
      let axiosAPI = async (config) => {
        let response = await axios.request(config);
        return response;        
      }
      let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://134.209.106.40:8000/devcamp-pizza365/drinks',
        headers: {},
      };
    
      axiosAPI(config)
        .then((response) => {
          console.log(response.data.message)
          dispatch({
            type: 'SET_ARRAY_DRINKS',
            payload: {
              drinksArray: response.data.message
            }
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }, [dispatch]);

    const handleClick = () => {
      setOpen(true);
    }

    const handleClose = () => {
      setOpen(false)
    }
    return(
        <>
        <div style={{
                textAlign: 'center',
                color: '#ff784e',
                paddingBottom: '2rem'
            }}>
                <h1>Chọn đồ uống</h1>
                <hr style={{
                    width:'20%',
                    border: '1px solid',
                }}></hr>
            </div>
            <div>
                <FormControl sx={{ m: 1, width: '100%' }}>
                    <InputLabel id="demo-multiple-name-label">Chọn loại nước</InputLabel>
                    <Select
                    labelId="demo-multiple-name-label"
                    id="demo-multiple-name"
                    input={<OutlinedInput label="drink-select" />}
                    MenuProps={MenuProps}
                    defaultValue={'COCA'}
                    >
                    {drinksArray.map((drink, index) => (
                        <MenuItem
                        key={index}
                        value={drink.maNuocUong}
                        onClick={() => handleSelcectDrinks(index)}
                        >
                        {drink.tenNuocUong}
                        </MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    {'Bạn đã chọn loại nước: '}{selectDrink.tenNuocUong}
                </Alert>
            </Snackbar>
            </div>
        </>
    )
}

export default DrinkSelect;