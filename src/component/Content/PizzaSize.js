import styled from '@emotion/styled';
import {Typography, Button, CardActions, Card, CardContent, Grid, CardHeader, Divider, Container, Snackbar, Alert} from '@mui/material';
import { deepOrange, grey } from '@mui/material/colors';
import { StarBorder } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

function PizzaSize () {
    
    const SpanInfo = styled('span')(() => ({
        fontWeight: 'bold'
    }))

    const SpanPrice = styled('span')(() => ({
        fontSize: 30,
        fontWeight: 'bolder',
        fontFamily: 'REM'
    }))

    const dispatch = useDispatch();

    const {SizeArray, sizePizza} = useSelector((reduxData) => reduxData.pizzaSizeReducer);
    const [open, setOpen] = useState(false);
    
    const handleButtonSelectPizzaSize = (index) => {
        dispatch({
            type: 'SELECTED_PIZZA_SIZE',
            payload: {
                size: SizeArray[index],
            }
        })
        handleClick()
    }

    const handleClick = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false)
    }

    useEffect(() => {
        //console.log(sizePizza)
    })
        
    return(
        <Container maxWidth='lg'>
            <div style={{
                textAlign: 'center',
                color: '#ff784e',
            }}>
                <h1>Chọn size pizza</h1>
                <hr style={{
                    width:'20%',
                    border: '1px solid'
                }}></hr>
                <Typography
                    sx={{
                        m: 3
                    }}
                >
                Chọn combo pizza phù hợp với nhu cầu của bạn
                </Typography>
            </div>
            <Grid container spacing={{xs:2, sm: 2, md: 4}} columns={{xs: 4, sm: 12, md: 12}}>
                {SizeArray.map((item, index) => {
                    return(
                        <Grid item xs={4} sm={item.pizzaSize === 'L (Large)' ? 12 : 6} md={4} key={index}>
                    <Card 
                        sx={{
                            minWidth: 275,
                            border: `1px solid ${grey[400]}`,
                            }}>
                        <CardHeader 
                                title= {item.pizzaSize}
                                subheader= {item.subheader}
                                subheaderTypographyProps={{
                                    align: 'center',
                                    color: grey[50]
                                  }}
                                sx={{
                                    bgcolor: item.pizzaSize === 'M (Medium)' ? deepOrange[700] : deepOrange[400],
                                    height: '40px',
                                }} 
                                action={item.pizzaSize === 'M (Medium)' ? <StarBorder 
                                        sx={{
                                            color: grey[50],
                                        }}
                                /> : null} 
                                titleTypographyProps={{ align: 'center', color: grey[50]}} 
                            />
                        <CardContent> 
                            <Typography sx={{
                                textAlign:'center',
                                m: 1
                            }}>
                                Đường kính: <SpanInfo>{item.duongKinh}</SpanInfo>
                            </Typography> 
                            <Divider />
                            <Typography sx={{
                                textAlign:'center',
                                m: 1
                            }}>
                                Sườn nướng: <SpanInfo>{item.suongNuong}</SpanInfo>
                            </Typography>
                            <Divider />
                            <Typography sx={{
                                textAlign:'center',
                                m: 1
                            }}>
                                Salad: <SpanInfo>{item.salad}</SpanInfo>
                            </Typography>
                            <Divider />
                            <Typography sx={{
                                textAlign:'center',
                                m: 1
                            }}>
                                Nước ngọt: <SpanInfo>{item.nuocNgot}</SpanInfo>
                            </Typography>
                            <Divider />
                            <Typography sx={{
                                textAlign: 'center',
                                m:1
                            }}>
                                <SpanPrice>{item.price}</SpanPrice> <br/>VNĐ
                            </Typography>
                        </CardContent>
                        <CardActions sx={{
                            border: '1px solid grey[400]',
                            bgcolor: 'grey[100]',
                        }}>
                            <Button size='large'
                            sx={{
                                width:' 100%',
                                color: grey[50],
                                bgcolor: sizePizza === SizeArray[index] ? deepOrange[700] : deepOrange[400],
                                '&:hover': {
                                    bgcolor: deepOrange[700]
                                }
                            }}
                            onClick={() => handleButtonSelectPizzaSize(index)}
                            >Chọn</Button>
                        </CardActions>
                    </Card>
                </Grid>
                    )
                })}
            </Grid>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    {'Bạn đã chọn size Pizza với mã : '}{sizePizza.pizzaSize}
                </Alert>
            </Snackbar>
        </Container>
    )
}

export default PizzaSize;