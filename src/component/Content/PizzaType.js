import {Typography, Button, CardActions, Card, CardContent, Grid, CardHeader,  CardMedia, Snackbar, Alert} from '@mui/material';
import { deepOrange, grey } from '@mui/material/colors';
import { useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';

function PizzaType () {
    const {TypeArray, typePizza } = useSelector((reduxData) => reduxData.pizzaTypeReducer);
    const [open, setOpen] = useState(false)

    const dispatch = useDispatch();

    const handleClickSelectTypePizza = (index) => {
        dispatch({
            type: 'SELECTED_PIZZA_TYPE',
            payload:{
                type: TypeArray[index]
            }
        })
        handleClick()
    }

    const handleClick = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false)
    }

    useEffect(() => {
        //console.log(typePizza)
    }, [typePizza])
    return(
        <div>
            <div style={{
                textAlign: 'center',
                color: '#ff784e',
                paddingBottom: '2rem'
            }}>
                <h1>Chọn loại pizza</h1>
                <hr style={{
                    width:'20%',
                    border: '1px solid',
                }}></hr>
            </div>
            <Grid
                sx={{
                    fontFamily: 'REM',
                }}
                container
                spacing={{ xs: 2, sm: 2 }}
                columns={{ xs: 4, sm: 12, md: 12 }}
                >
                {TypeArray.map((item, index) => (
                    <Grid
                    item
                    xs={4}
                    sm={item.maTypePizza === 'CHESSY CHICKEN BACON' ? 12 : 6}
                    md={4}
                    key={index}
                    >
                    <Card
                        sx={{
                        minWidth: 275,
                        border: `1px solid ${grey[400]}`,
                        maxHeight: '100%'

                        }}
                    >
                        <CardMedia component='img' image={item.image} />
                        <CardHeader
                        title={
                            <>
                            <Typography
                                variant='h6'
                                sx={{
                                fontWeight: 'bold',
                                }}
                            >
                                {item.maTypePizza}
                            </Typography>
                            <Typography
                                variant='body1'
                                sx={{
                                paddingTop: '1rem',
                                }}
                            >
                                {item.subTitle}
                            </Typography>
                            </>
                        }
                        />
                        <CardContent>{item.description}</CardContent>
                        <CardActions
                        sx={{
                            border: `1px solid ${grey[400]}`,
                            bgcolor: `${grey[100]}`,
                        }}
                        >
                        <Button
                            size='large'
                            sx={{
                            width: '100%',
                            bgcolor: typePizza === TypeArray[index] ? deepOrange[700] : deepOrange[400],
                            color: grey[50],
                                '&:hover': {
                                bgcolor: deepOrange[700],
                                },
                            }}
                            onClick={() => handleClickSelectTypePizza(index)}
                        >
                            Chọn
                        </Button>
                        </CardActions>
                    </Card>
                    </Grid>
                ))}
                </Grid>
                <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    {'Bạn đã chọn loại Pizza với mã : '}{typePizza.maTypePizza}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default PizzaType;