import React from "react";
import Slider from "react-slick";
import '../../../node_modules/slick-carousel/slick/slick.css'
import '../../../node_modules/slick-carousel/slick/slick-theme.css'
import image from "../../asset/images/image.js";

function Lading () {

    const settings = {
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: true,
        initialSlide: 2,
        autoplay: true,
      };

    return (
        <div style={{padding: '1rem'}}>
            <Slider {...settings}>
            <div>
                <img width={'100%'} src={image.image1} alt="slide1"/>
            </div>
            <div>
                <img width={'100%'} src={image.image2} alt="slide1"/>
            </div>
            <div>
                <img width={'100%'} src={image.image3} alt="slide1"/>
            </div>
            <div>
                <img width={'100%'} src={image.image4} alt="slide1"/>
            </div>
            </Slider>
        </div>
    );
}
export default Lading;