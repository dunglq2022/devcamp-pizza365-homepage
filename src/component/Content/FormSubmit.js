import * as React from 'react';
import axios from 'axios';
import {Grid, TextField, Button, Box, Modal, Typography, Divider} from '@mui/material/';
import { deepOrange, grey } from '@mui/material/colors';
import styled from '@emotion/styled';
import { useSelector } from 'react-redux';

const styleButtonConfirm = () => ({
    backgroundColor: `${deepOrange[400]}`,
    color: `${grey[50]}`,
    '&:hover': {
        backgroundColor:`${deepOrange[700]}`
    },
    fontWeight: 'bold',
    fontSize: '16px',
    mt: 3,
    left: '25%'
    
})

const styleModal = () => ({
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 20,
    p: 4,           
});

const SpanBold = styled('span')({
    fontWeight:'bold'
});

function FormSubmit () {
    const {sizePizza} = useSelector((reduxData) => reduxData.pizzaSizeReducer);
    const {typePizza} = useSelector((reduxData) => reduxData.pizzaTypeReducer);
    const [fullName, setFullName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [phone, setPhone] = React.useState(0)
    const [address, setAddress] = React.useState('');
    const [voucher, setVoucher] = React.useState(0)
    const [note, setNote] = React.useState('');
    const [error, setError] = React.useState('',)
    const [openShowOrder, setOpenShowOrder] = React.useState(false);
    const [openShowConfirm, setOpenShowConfirm] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [percent, setPercent] = React.useState(20);
    const handleOpenShowOrder = () => setOpenShowOrder(true);
    const handleCloseShowOrder = () => setOpenShowOrder(false);
    const handleOpenShowError = () => setOpen(true);
    const handleCloseShowError = () => setOpen(false);
    const handleOpenShowConfirm = () => setOpenShowConfirm(true);
    const handleCloseShowConfirm = () => setOpenShowConfirm(false);
    const thanhTien = (sizePizza.price - (sizePizza.price * percent)/100)
    
    const handleOnChangeFullName = (event) => {
        setFullName(event.target.value);
        //console.log(fullName);
    }

    const handleOnChangeEmail = (event) => {
        setEmail(event.target.value);
        //console.log(email);
    }
    
    const handleOnChangePhone = (event) => {
        setPhone(event.target.value);
        //console.log(phone);
    }

    const handleOnChangeAddress = (event) => {
        setAddress(event.target.value);
        //console.log(address);
    }

    const handleOnChangeVoucher = (event) => {
        setVoucher(event.target.value);
        //console.log(voucher);
    }

    const handleOnChangeNote = (event) => {
        setNote(event.target.value);
        //console.log(note);
    }

    const handleButtonSendOrder = () => {
        const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        const phoneNumberRegex = /^[0-9]+$/;

        if (!fullName ) {
            setError('Please enter your name');
            handleOpenShowError()
            return false
        }

        if (!email) {
            setError('Please enter your email');
            handleOpenShowError()
            return false;
        }

        if (!emailRegex.test(email)) {
            setError('Email invalid!')
            handleOpenShowError()
            return false;
        }

        if (!phone) {
            setError('Please enter your number phone')
            handleOpenShowError()
            return false;
        }

        if (!phoneNumberRegex.test(phone)) {
            setError('Number phone invalid!')
            handleOpenShowError()
            return false;
        }
        
        if (!address) {
            setError('Please enter your address')
            handleOpenShowError()
            return false;
        }

        if (!voucher) {
           setVoucher(' ')
        } else {         
            checkVoucher()
        }
        handleOpenShowOrder()

        //Gọi API xử lý button xác nhận đơn hàng
        const data = JSON.stringify({
            "kichCo": sizePizza.pizzaSize,
            "duongKinh":sizePizza.duongKinh,
            "suon": sizePizza.suongNuong,
            "salad": sizePizza.salad,
            "loaiPizza": typePizza.title,
            "idVourcher": voucher,
            "soLuongNuoc": sizePizza.nuocNgot,
            "hoTen": fullName,
            "thanhTien": thanhTien,
            "email": email,
            "soDienThoai": phone,
            "diaChi": address,
            "loiNhan": note
        });

        const config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: { 
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios.request(config)
            .then((response => {
                console.log(response);
            }))
            .catch((error) => {
                //console.log(error);
            });
    }
    
    const handleButtonConfirmOrder = () => {
        handleCloseShowOrder();
        handleOpenShowConfirm();
     }

    //Kiểm tra voucher có tồn tại hay không?   
    const checkVoucher = async () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `http://localhost:8000/devcamp-pizza365/vouchers/${voucher}`,
            headers: { }
          };
          
        await axios.request(config)
            .then((response) => {
                let data = response.data.data
                console.log(data[0].phamTramGiamGia)
                setPercent(data[0].phamTramGiamGia)
            })
            .catch((error) => {
                console.log(error);
        });
}

    return(
        <>
            <div style={{
                textAlign: 'center',
                color: '#ff784e',
                paddingBottom: '2rem'
            }}>
                <h1>Gửi đơn hàng</h1>
                <hr style={{
                    width:'20%',
                    border: '1px solid',
                }}></hr>
            </div>
            <React.Fragment>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                    <TextField
                        required
                        id="fullName"
                        name="fullName"
                        label="Họ và tên"
                        fullWidth
                        autoComplete="given-name"
                        variant="outlined"
                        helperText="Vui lòng nhập đầy đủ họ tên"
                        onChange={handleOnChangeFullName}
                    />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                    <TextField
                        required                        
                        id="lastName"
                        name="email"
                        label="Email"
                        fullWidth
                        autoComplete="email"
                        variant="outlined"
                        helperText="Vui lòng nhập đúng định dạng email"
                        onChange={handleOnChangeEmail}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        required
                        id="numberphone"
                        name="numberphone"
                        label="Số điện thoại"
                        fullWidth
                        autoComplete="tel"
                        variant="outlined"
                        helperText="Vui lòng nhập số điện thoại"
                        onChange={handleOnChangePhone}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        required
                        id="address"
                        name="address"
                        label="Địa chỉ"
                        fullWidth
                        autoComplete="shipping street-address"
                        variant="outlined"
                        helperText="Vui lòng nhập địa chỉ"
                        onChange={handleOnChangeAddress}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        id="voucher"
                        name="voucher"
                        label="Mã giảm giá"
                        fullWidth
                        autoComplete="shipping voucher"
                        variant="outlined"
                        onChange={handleOnChangeVoucher}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        id="note"
                        name="note"
                        label="Ghi chú"
                        fullWidth
                        autoComplete="shipping note"
                        variant="outlined"
                        onChange={handleOnChangeNote}
                    />
                    </Grid>
                    <Grid item xs={12} sx={{
                         display: 'flex',
                         justifyContent: 'center',
                    }}>
                        <Button 
                            type="submit"
                            sx={{
                                mb: 3,
                                width:' 100%',
                                color: grey[50],
                                bgcolor: deepOrange[400],
                                '&:hover': {
                                    bgcolor: deepOrange[700]
                                }
                            }}
                            onClick={handleButtonSendOrder}
                            >
                                Gửi đơn hàng
                        </Button>
                    </Grid>
                </Grid>
                <Modal
                    open={openShowOrder}
                    onClose={handleCloseShowOrder}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    id='modal-show-order-complete'
                    >
                    <Box sx={styleModal}>
                        <Typography sx={{
                            fontFamily: 'REM'
                        }} align='center' id="modal-modal-title" variant="h4" component="h2">
                        Thông tin đơn hàng
                        </Typography>
                        <Typography sx={{ mt: 2, mb: 2}}>
                            <SpanBold>{'Size Pizza: '}</SpanBold>{sizePizza.pizzaSize}
                        <Typography sx={{ mt: 2}}>
                            <SpanBold>{'đường kính: '}</SpanBold>{sizePizza.duongKinh} {'cm'}
                            <SpanBold>{', sườn nướng: '}</SpanBold>{sizePizza.suongNuong}
                        </Typography>
                        <Typography>
                            <SpanBold>{'salad: '}</SpanBold>{sizePizza.salad} {'gram'}
                            <SpanBold>{', nước uống: '}</SpanBold>{sizePizza.nuocNgot} {'ly'}
                        </Typography>
                        <Typography>
                            <SpanBold>{'Giá Combo '}</SpanBold>{sizePizza.price} {'đồng'}
                        </Typography>
                        </Typography>
                        <Divider></Divider>
                        <Typography sx={{ mt: 2, mb: 2}}>
                            <SpanBold>{' Loại Pizza: '}</SpanBold>{typePizza.tenTypePizza}   
                        </Typography>
                        <Typography sx={{ mt: 2, mb: 2}}>
                            <SpanBold>{' mô tả: '}</SpanBold>{typePizza.description}   
                        </Typography>
                        <Divider></Divider>
                        <Typography sx={{ mt: 2 }}>
                            <SpanBold>{'Họ và tên: '}</SpanBold>{fullName}
                        </Typography>
                        <Typography sx={{ mt: 2 }}>
                            <SpanBold>{'Email: '}</SpanBold>{email}
                        </Typography>
                        <Typography sx={{ mt: 2 }}>
                            <SpanBold>{'Số điện thoại: '}</SpanBold>{phone}
                        </Typography>
                        <Typography sx={{ mt: 2 }}>
                            <SpanBold>{'Địa chỉ: '}</SpanBold>{address}
                        </Typography>
                        <Typography sx={{ mt: 2 }}>
                            {voucher === ' ' ? <SpanBold>{'Không áp mã giảm giá '}</SpanBold> : <><SpanBold>{'Mã giảm giá: '}</SpanBold>{voucher}<SpanBold>{', áp dụng giảm giá: '}</SpanBold>{percent}%</>}                            
                        </Typography>
                        <Typography sx={{ mt: 2 }}>
                            <SpanBold>{'Ghi chú: '}</SpanBold>{note}
                        </Typography>
                        <Divider></Divider>
                        <Typography sx={{ mt: 2, fontSize: 22}}>
                            <SpanBold >{'Tổng tiền: '}</SpanBold>{(thanhTien.toLocaleString('vi-VN'))} {'đồng'}
                        </Typography>
                        <Button onClick={handleButtonConfirmOrder} sx={styleButtonConfirm}>Xác nhận đơn hàng</Button>
                    </Box>
                </Modal>
                <Modal
                    open={openShowConfirm}
                    onClose={handleCloseShowConfirm}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    id='modal-confirm-order'
                >
                    <Box sx={styleModal}>
                    <Typography id="modal-modal-title" align='center' variant="h6" component="h2">
                         {'Xác nhận thành công'}
                    </Typography>
                    </Box>
                </Modal>
                <Modal
                    open={open}
                    onClose={handleCloseShowError}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={styleModal}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Error
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        {error}
                    </Typography>
                    </Box>
                </Modal>
            </React.Fragment>
        </>
    )
}
export default FormSubmit;