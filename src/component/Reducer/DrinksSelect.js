
const initialState = {
    drinksArray: [],
    selectDrink: ''
}

const DrinkSelect = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECT_DRINK': {
            return {
                ...state,
                selectDrink: action.payload.selectDrink
            }
        }
        case 'SET_ARRAY_DRINKS': {
            return {
                ...state,
                drinksArray: action.payload.drinksArray
            }
        }
        default: {
            return state;
        }
    }
}

export default DrinkSelect;