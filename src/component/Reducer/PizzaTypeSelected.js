import image from '../../asset/images/image';

const initialState = {
    TypeArray : [
        {
            image:image.bacon,
            maTypePizza: 'OCEAN MANIA',
            tenTypePizza: 'Hải sản',
            subTitle: 'PIZZA HAI SẢN XỐT MAYONASE.',
            description:'Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua,Hành Tây.'
        },
        {
            image:image.hawaii,
            maTypePizza: 'HAWAIIAN',
            tenTypePizza: 'Hawaii',
            subTitle: 'PIZZA DĂM BÔNG ĐỨA KIỂU HAWAII.',
            description:'Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.'
        },
        {
            image:image.seaFood,
            maTypePizza: 'CHESSY CHICKEN BACON',
            tenTypePizza: 'Thịt hun khói',
            subTitle: 'PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI.',
            description:'Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.'
        }
    ],
    typePizza: ''
}

const PizzaTypeSelected = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECTED_PIZZA_TYPE': {
            return {
                ...state,
                typePizza: action.payload.type
            }
        }
        default: {
            return state;
        }
    }
}

export default PizzaTypeSelected