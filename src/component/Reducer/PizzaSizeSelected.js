const initialState = {
    SizeArray : [
        {
            pizzaSize: 'S',
            duongKinh: 20,
            suongNuong: 2,
            salad: 200,
            nuocNgot: 2,
            price: 150000
        },
        {
            pizzaSize: 'M',
            subheader: 'Best Seller',
            duongKinh: 25,
            suongNuong: 4,
            salad: 300,
            nuocNgot: 3,
            price: 200000,
        },
        {
            pizzaSize: 'L',
            duongKinh: 30,
            suongNuong: 8,
            salad: 500,
            nuocNgot: 4,
            price: 250000
        }
    ],
    sizePizza: ''
}

const PizzaSizeSelected = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECTED_PIZZA_SIZE': {
            return {
                ...state,
                sizePizza: action.payload.size
            }
        }
        default: {
            return state;
        }
    }
}

export default PizzaSizeSelected;