
import HeaderComponent from '../component/Header/HeaderComponent'
import ContentComponent from '../component/Content/Content'
import Footer from '../component/Content/Footer'

function Home() {
  return (
    <>
      <HeaderComponent/>
      <ContentComponent/>
      <Footer/>
    </>
  );
}

export default Home;
