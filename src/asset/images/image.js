import img1 from '../images/1.jpg'
import img2 from '../images/2.jpg'
import img3 from '../images/3.jpg'
import img4 from '../images/4.jpg'
import bacon from '../images/bacon.jpg'
import hawaii from '../images/hawaiian.jpg'
import saladBar from '../images/feature-saladbar.jpg'
import seaFood from '../images/seafood.jpg'

const image = {
    image1: img1,
    image2: img2,
    image3: img3,
    image4: img4,
    bacon: bacon,
    hawaii: hawaii,
    saladBar: saladBar,
    seaFood: seaFood
}

export default image
