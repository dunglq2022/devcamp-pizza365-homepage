import {Route, Routes } from 'react-router-dom';
import Home from './pages/Home'
import Admin from './pages/Admin';

function App() {
  return (
    <div>
      <Routes>
          <Route path="/" Component = {Home}/>
          <Route path="*" Component = {Home}/>
          <Route path='/admin' Component={Admin}/>        
      </Routes>
    </div>
  );
}

export default App;
