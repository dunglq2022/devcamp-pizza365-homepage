import { combineReducers, createStore } from "redux";
import pizzaSizeSelected from '../component/Reducer/PizzaSizeSelected'
import pizzaTypeSelected from '../component/Reducer/PizzaTypeSelected'
import drinkSelect from '../component/Reducer/DrinksSelect'

const appReducer = combineReducers({
    pizzaSizeReducer: pizzaSizeSelected,
    pizzaTypeReducer: pizzaTypeSelected,
    drinkListReducer: drinkSelect,
});

const store = createStore(
    appReducer
)

export default store;